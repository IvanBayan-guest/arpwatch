Description: add option -z ignorenet/ignoremask
 Used to set a range of ip addresses to ignore (such as a DHCP range).
 Netmask is specified as 255.255.128.0.
Author: KELEMEN Péter <fuji@debian.org>
Author: Sebastian Reichelt <SebastianR@gmx.de>
Bug-Debian: https://bugs.debian.org/289135

--- a/arpwatch.8
+++ b/arpwatch.8
@@ -110,6 +110,14 @@
 ]
 .\" **
 .\" **
+.br
+.ti +8
+[
+.B -z
+.I ignorenet/ignoremask
+]
+.\" **
+.\" **
 .ad
 .SH DESCRIPTION
 .B Arpwatch
@@ -237,6 +245,16 @@
 .\" **
 .\" **
 .LP
+(Debian) The
+.B -z
+option is used to set a range of ip addresses to ignore (such as a DHCP
+range).  Both the ignorenet and the ignoremask are specified in
+numbers-and-dots notation and separated from each other by a slash (/).
+Specifying the ignoremask by subnet length is not supported.  If the ignoremask
+is omitted, 255.255.255.255 is assumed.  Example: -z 192.168.10.0/255.255.255.0
+.\" **
+.\" **
+.LP
 Note that an empty
 .I arp.dat
 file must be created before the first time you run
--- a/arpwatch.c
+++ b/arpwatch.c
@@ -125,6 +125,9 @@
 static int nets_ind;
 static int nets_size;
 
+static struct in_addr ignore_net;
+static struct in_addr ignore_netmask;
+
 extern int optind;
 extern int opterr;
 extern char *optarg;
@@ -213,7 +216,11 @@
 		"Q"
 		/**/
 		/**/
+		"z:"
+		/**/
+		/**/
 	;
+	char *tmpptr;
 
 	if (argv[0] == NULL)
 		prog = "arpwatch";
@@ -231,6 +238,9 @@
 	interface = NULL;
 	rfilename = NULL;
 	pd = NULL;
+
+	inet_aton("0.0.0.0", &ignore_net);
+	inet_aton("255.255.255.255", &ignore_netmask);
 	while ((op = getopt(argc, argv, options)) != EOF)
 		switch (op) {
 
@@ -299,6 +309,32 @@
 
 		/**/
 		/**/
+		case 'z':
+			tmpptr = strtok(optarg, "/");
+			if (!tmpptr) {
+				fprintf(stderr, "%s: Need argument after -z\n", prog);
+				usage();
+			}
+			if (!inet_aton(tmpptr, &ignore_net)) {
+				fprintf(stderr, "%s: Not a valid IPv4 address (in -z argument): %s\n", prog, tmpptr);
+				usage();
+			}
+			tmpptr = strtok(NULL, "/");
+			if (tmpptr)
+				if (!inet_aton(tmpptr, &ignore_netmask)) {
+					fprintf(stderr, "%s: Not a valid netmask (in -z argument): %s\n", prog, tmpptr);
+					usage();
+				}
+				// make sure -z argument does not contain more slashes
+				if (strtok(NULL, "/")) {
+					fprintf(stderr, "%s: Error parsing -z argument\n", prog);
+					usage();
+				}
+				inet_aton(tmpptr, &ignore_netmask);
+			ignore_net.s_addr &= ignore_netmask.s_addr;
+			break;
+		/**/
+		/**/
 		default:
 			usage();
 		}
@@ -501,7 +537,15 @@
 		return;
 	}
 
+	/* Ignores the specified netmask/metwork */
+	if ((sia & ignore_netmask.s_addr) == ignore_net.s_addr) {
+		if (debug) {
+			dosyslog(LOG_INFO, "ignored", sia, sea, sha, interface);
+		}
+		return;
+	}
 	/* Got a live one */
+
 	t = h->ts.tv_sec;
 	can_checkpoint = 0;
 	if (!ent_add(sia, sea, t, NULL, interface))
@@ -888,6 +932,9 @@
 		"[-Q] "
 		/**/
 		/**/
+		"[-z ignorenet/ignoremask] "
+		/**/
+		/**/
 		"\n"
 	;
 
