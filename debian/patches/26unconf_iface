Description: assume unconfigured interface rather than exit
Author: Lukas Schwaighofer <lukas@schwaighofer.name>

if the interface is unconfigured, arpwatch can still work as normal.
Only automatically detecting which packets do not belong to the
configured subnet (bogons) is not possible.

To allow for useful bogon processing nonetheless, we set an impossible
net/netmask combination; networks can instead be added using the -n
option.

Based on a patch from KELEMEN Péter <fuji@debian.org>
---
 arpwatch.c | 16 +++++++++++++---
 1 file changed, 13 insertions(+), 3 deletions(-)

--- a/arpwatch.c
+++ b/arpwatch.c
@@ -357,9 +357,19 @@
 
 		/* Determine network and netmask */
 		if (pcap_lookupnet(interface, &net, &netmask, errbuf) < 0) {
-			(void)fprintf(stderr, "%s: bad interface %s: %s\n",
-			    prog, interface, errbuf);
-			exit(1);
+			syslog(LOG_INFO,
+				"Could not get IPv4 address on %s for bogon detection",
+				interface);
+			if (!allsubnets && nets_ind == 0) {
+				syslog(LOG_ERR,
+					"No subnets to watch configured; either use -a to process bogons or watch subnets explicitly using -n network/mask");
+				exit(1);
+			}
+			// the net/netmask settings below are intentionally set in a way
+			// that no address is contained in the "subnet" (all packets
+			// outside of networks given using -n are considered to be bogons)
+			net = 1;
+			netmask = 0;
 		}
 
 		/* Drop into the background if not debugging */
